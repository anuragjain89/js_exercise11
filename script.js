var DomainAndSubDomainFinder = function (url) {
  this.url = url;
};

DomainAndSubDomainFinder.prototype.URL_FILTER = /(?:ht|f)tps?\:\/\/[^\/]*/im;
DomainAndSubDomainFinder.prototype.SUBDOMAIN_FILTER = /((\S+)\.)?(\S+\.\S+)$/im;
DomainAndSubDomainFinder.prototype.getDomainAndSubDomainAsObject = function () {
  if (!this.url.match(this.URL_FILTER)) {
    alert('Please enter a valid URL.');
  } else {
    var resultArray = this.url.match(this.URL_FILTER)[0].split('\/\/')[1].match(this.SUBDOMAIN_FILTER);
    return {
      subdomain: resultArray[2],
      domain: resultArray[3]
    };
  }
};

DomainAndSubDomainFinder.prototype.getDomainAndSubDomainAsString = function () {
  var resultObject = this.getDomainAndSubDomainAsObject(this.url);
  var message = 'Domain: ' + resultObject.domain;
  if (resultObject.subdomain && (resultObject.subdomain !== 'www')) {
    message += ', Subdomain: ' + resultObject.subdomain;
  }
  return message;
};

var alertDomainAndSubdomain = function () {
  var message = new DomainAndSubDomainFinder(this.querySelector('#urlfield').value.trim()).getDomainAndSubDomainAsString();
  alert(message);
};
document.forms[0].addEventListener('submit', alertDomainAndSubdomain);
